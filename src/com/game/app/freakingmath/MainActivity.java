package com.game.app.freakingmath;

import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private Button mBtnRate;

	private ImageView mImgTop;

	private ImageView mImgPlay;

	private Context mContext = this;
	private static long back_pressed;

	public static int adId = 65824976;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		this.mBtnRate = (Button) this.findViewById(R.id.btnRate);
		this.mImgTop = (ImageView) this.findViewById(R.id.imgTop);
		this.mImgPlay = (ImageView) this.findViewById(R.id.imgPlay);

		this.mBtnRate.setOnClickListener(this);
		this.mImgTop.setOnClickListener(this);
		this.mImgPlay.setOnClickListener(this);

		// Create the interstitial.
		loadAdview();

	}
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		loadAdview();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();

		switch (id) {
		case R.id.btnRate:
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("market://details?id=" + getPackageName())));
			break;
		case R.id.imgTop:
			startActivity(new Intent(mContext, TopScoreActivity.class));
			break;
		case R.id.imgPlay:
			startActivity(new Intent(mContext, PlayActivity.class));
			break;

		}
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		loadAdview();
		// If the user is looking at detailed rows, put them back to the other screen instead
		// of leaving the page entirely
		if (back_pressed + 2000 > System.currentTimeMillis())
		{
			super.onBackPressed();
		}
		else
		{
			Toast.makeText(this, getString(R.string.doubleClickMsg_en), Toast.LENGTH_SHORT).show();
			back_pressed = System.currentTimeMillis();
		}
	}

}
