package com.game.app.freakingmath;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.game.app.freakingmath.R;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

public class TopScoreActivity extends Activity implements OnClickListener {
	public static int adId = 65832139;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.best_score_main);

		TextView tv = (TextView)findViewById(R.id.topScore);
		int bestScore = PreferenceUtil.getValue(this, Var.KEY_BEST_SCORE, 0);
		tv.setText("Best Score : " + String.valueOf(bestScore));
		// Create the interstitial.
		loadAdview();

	}
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		loadAdview();
		super.onBackPressed();
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

}
