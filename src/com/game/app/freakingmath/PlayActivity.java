package com.game.app.freakingmath;

import java.util.Random;

import com.game.app.freakingmath.R;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PlayActivity extends Activity {

	private Context mContext = this;

	private TextView mTvNum;

	private TextView mTvTime;

	private ImageView mImgTick;

	private ImageView mImgDel;

	private TextView mTv1;

	private TextView mTv2;

	private int mNum = 0;
	private RelativeLayout mainform;
	static boolean exactly = false;
	private String condition = "";
	public CountDown countDown;

	private boolean finish = false;
	public static int adId = 65824976;
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	private Handler failt = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			if (finish) {
				return;
			}

			PreferenceUtil.setValue(mContext, Var.KEY_SCORE, mNum);

			mNum = 0;
			
			SoundUtil.hexat(mContext, SoundUtil.DIA);
			if (countDown.isFinish()){
				condition = "Out of time!";
			}
			EndDialog endDialog = new EndDialog(mContext, handler, condition);
			endDialog.show();

			finish = true;

		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_play);

		mTvNum = (TextView) this.findViewById(R.id.tvNum);
		mTvTime = (TextView) this.findViewById(R.id.tvTime);
		mTv1 = (TextView) this.findViewById(R.id.tv1);
		mTv2 = (TextView) this.findViewById(R.id.tv2);

		mImgTick = (ImageView) this.findViewById(R.id.imgTick);
		mImgDel = (ImageView) this.findViewById(R.id.imgDel);
		mImgTick.setOnClickListener(onClickListener);
		mImgDel.setOnClickListener(onClickListener);
		mainform = (RelativeLayout)this.findViewById(R.id.mainform);
		countDown = new CountDown();
		countDown.setTvTime(mTvTime);
		countDown.setOnTickHtmlListener(failt);
		loadAdview();
		initFreakingMath();
	}

	@SuppressLint("NewApi")
	private void initFreakingMath() {
		Random random = new Random();
		int i = random.nextInt(6);
		if (i == 0){
			mainform.setBackgroundResource(R.color.maucam);
		}else if (i == 1){
			mainform.setBackgroundResource(R.color.xanhduong);
		}else if (i == 2){
			mainform.setBackgroundResource(R.color.xanhla);
		}else if (i == 3){
			mainform.setBackgroundResource(R.color.xanhnhat);
		}else if (i == 4){
			mainform.setBackgroundResource(R.color.timhong);
		}else if (i == 5){
			mainform.setBackgroundResource(R.color.timthan);
		}else if (i == 6){
			mainform.setBackgroundResource(R.color.timxanh);
		}else if (i == 6){
			mainform.setBackgroundResource(R.color.camnhat);
		}
		mNum += 1;
		FreakingMath freakingMath = FreakingMath.randomFreakingMath();
		exactly = freakingMath.isExactly();

		mTvNum.setText(mNum + "");
		mTvTime.setText("2s");
		mTv1.setText(freakingMath.getLeft() + " " + freakingMath.getOpt() + " "
				+ freakingMath.getRight());
		mTv2.setText("= " + freakingMath.getResult());
		condition = freakingMath.getLeft() + " " + freakingMath.getOpt() + " "
				+ freakingMath.getRight() + "= " + freakingMath.getResult();
//		mTv2.setRotation(90);
		
		countDown.tick();
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String object = (String) v.getTag();
			boolean b = Boolean.parseBoolean(object);

			if (exactly == b) {
				initFreakingMath();
				SoundUtil.hexat(mContext, SoundUtil.WINSOUND);
			} else {
				failt.sendEmptyMessage(0);
			}
		}
	};

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			finish();
		}
	};
	
	protected void onDestroy() {
		if(countDown.timer!=null){
			countDown.timer.cancel();
		}
		super.onDestroy();
		
	};

}
